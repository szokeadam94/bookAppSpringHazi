package hu.mik.java2.book.logging;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Service;

@Aspect
@Service
public class LoggingAspect {

	public LoggingAspect() {
		System.out.println("Logger initialized");
	}

	@Around("execution(* hu.mik.java2.book.service.*.*(..))")
	public Object log(ProceedingJoinPoint joinPoint) {
		System.out.println("hijacked: " + joinPoint.getSignature());
		System.out.println("hijacked: " + joinPoint.getArgs());

		Object returnValue = null;
		try {
			returnValue = joinPoint.proceed();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return returnValue;
	}

}
