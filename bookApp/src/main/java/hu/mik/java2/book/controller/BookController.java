package hu.mik.java2.book.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import hu.mik.java2.book.bean.Book;
import hu.mik.java2.book.service.BookService;

@Controller
@RequestMapping("/booking")
public class BookController {

	@Autowired
	private BookService bookService;

	// @Autowired
	// public BookController(BookService bookService)
	// {
	// this.bookService = bookService;
	//
	// }

	@RequestMapping("/book_details")
	public String bookDetails(Model model, Integer bookId) {

		Book book = new Book();
		if (bookId != null) {
			book = this.bookService.getBookById(bookId);
		}

		model.addAttribute("book", book);

		return "book_details";
	}
	

	@RequestMapping("/book_list")
	public String bookList(Model model) {

		model.addAttribute("books", this.bookService.listBooks());

		return "book_list";
	}

	@RequestMapping("/book_edit")
	public String bookEdit(Model model, Integer bookId) {

		Book book = new Book();
		if (bookId != null) {
			book = this.bookService.getBookById(bookId);
		}

		model.addAttribute("book", book);

		return "book_edit";
	}

	
	@RequestMapping(value = "/book_edit", method = { RequestMethod.POST })
	public String bookEditPost(Book book, Model model) {

		if (book.getId() == null) {
			book.setId(null);
		}

		Book updateBook;

		if (book.getId() == null) {
			updateBook = this.bookService.saveBook(book);
		} else {
			updateBook = this.bookService.updateBook(book);
		}

		model.addAttribute("book", updateBook);

		return "book_details";
	}
	

	@RequestMapping("/book_delete")
	public String bookDelete(Model model, Integer bookId) {

		Book book = new Book();
		if (bookId != null) {
			book = this.bookService.getBookById(bookId);
		}

		model.addAttribute("book", book);

		return "book_delete";
	}

	
	@RequestMapping(value = "/book_delete", method = { RequestMethod.POST })
	public String bookDeletePost(Book book) {

		if (book != null) {
			this.bookService.deleteBook(book);
		}

		return "book_delete_successful";
	}

	
	@RequestMapping(value = "/book_list", method = { RequestMethod.POST })
	public String bookListPostFilterByAuthor(@RequestParam("filtervalue") String filterValue, Model model) {

		System.out.println("F: " + filterValue);
		if (this.bookService.bookListFiltered(filterValue).isEmpty()) {
			return "book_not_found";
		} else {
			model.addAttribute("books", this.bookService.bookListFiltered(filterValue));

			return "book_list";
		}

	}

}
